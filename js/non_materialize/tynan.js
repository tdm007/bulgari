/**
 * Created by Tynan on 17/04/2017.
 */

//All this needs to happen once page is loaded, to avoid rendering issues.
$(document).ready(function () {
    /*
     Determine which background to use in parallax divs.
     This seems a bit hacky, but Materialize does parallax
     using src attribute, and so it can't be done anywhere in CSS.
     992px is the border between tablet and desktop in Materialize.
     */
    if (window.innerWidth < 992) {
        $('.parallax-image').each(function () {
            this.setAttribute('src', 'img/13300706touch_optimized.jpg');
        });
    }
    else {
        $('.parallax-image').each(function () {
            this.setAttribute('src', 'img/13300706_optimized.jpg');
        });
    }

    //Initialize Materialize plugins once loaded
    $('.parallax').parallax();
    $('.slider').slider({indicators: true, interval: 6000});
    $(".button-collapse").sideNav({
        closeOnClick: true,
        draggable: true
    });

});
/*
 There's going to be lots happening when we scroll, so let's
 tie everything up into a master function. Set it to fire on
 scroll events, as well as resize and page load.
 */
var $window = $(window);
$window.on('scroll', masterScrollFunction);
$window.on('scroll resize', masterScrollFunction);
$window.trigger('scroll');

//Let's get cracking.
function masterScrollFunction() {

    var $top = $window.scrollTop(); //I don't think simple ints need to be jQuery vars?
    var $slideLogo = $('.main-logo');
    var $brownBlockTop = Math.floor($('.brown').offset().top);
    var $pushpinNav = $('.pushpin-nav');
    var $navLogo = $('.navbar-logo');
    var $menuIcon = $('#menu-icon');
    var $socmedFAB = $('a.btn-large');
    var $atTheBottom = (document.body.clientHeight - $window.height()) - 300;
    var $ringsTitle = $('#rings-title');
    var $collectionsTitle = $('#collections-title');

    //Slide that logo out to the left as page is scrolled.
    $slideLogo.css('right', $top + 'px');

    /*
     Handle displaying nav elements: menu icon and logo.
     I wish I knew why the animate property of the logo
     wasn't working with displaying. It should fade in.
     I've also used these blocks to display the relevant
     titles when the sections are scrolled to.
     */
    if ($pushpinNav.hasClass('pinned')) {
        $navLogo.css('display', 'block');
        $menuIcon.css('display', 'block');
        $ringsTitle.css('display', 'block');
        $collectionsTitle.css('display', 'block');
    } else {
        $navLogo.css('display', 'none');
        $menuIcon.css('display', 'none');
        $ringsTitle.css('display', 'none');
        $collectionsTitle.css('display', 'none');
    }

    /*
     transition needs to be applied here
     despite being used for background colour in the
     next block - because pushpin changes the position
     of the nav when it becomes fixed, if there is a
     transition, it will animate in from the top of the page
     instead of sticking directly. This can be taken care of
     by ensuring the transition property is only applied
     when the window has scrolled past the point
     that the nav becomes fixed -  the top of #pushpin1

     ...NOTE: THIS DOESN'T WORK if the user scrolls too quickly. Uargh.
     Add some kind of check? Milliseconds since last scroll event?
     if !(millisecondsSinceLastScroll > 50){...etc}

     ...I just fixed it by adding another conditional to the
     if statement. It makes more sense to transition based on when it is
     pinned AND where on the page we are scrolled - it was the fact it
     could have an transition and NOT be pinned that caused the issue.
     I decided to leave all this for the sake of posterity.
     Milliseconds... pff.
     */
    if ($top > $('#pushpin1').offset().top && $pushpinNav.hasClass('pinned')) {
        $pushpinNav.css('transition', 'all 2s ease');
    } else {
        $pushpinNav.css('transition', 'none');
    }

    /*
     Fade the background of the nav from grey to brown
     and back when scrolling between sections. I tried
     tying the colour values directly to the scroll
     position so the fade incremented with scrolling,
     but it was going to be far too difficult to achieve
     practically the same result with CSS.

     This also handles displaying the titles of the sections
     All these conditionals are getting ugly.
     */
    if (($top + 120) > $brownBlockTop) {
        $ringsTitle.css('display', 'none');
        $collectionsTitle.css('display', 'block');
        $pushpinNav.removeClass('grey', 'lighten-2');
        $pushpinNav.css('background-color', '#d7ccc8');
    } else {
        if ($pushpinNav.hasClass('pinned')){
            $ringsTitle.css('display', 'block');
            $collectionsTitle.css('display', 'none');
        }
        $pushpinNav.css('background-color', '#e0e0e0');
        $pushpinNav.addClass('grey', 'lighten-2');
    }

    /*
     Pulse and change the colour of the social media
     Floating Action Button when the user is near
     the bottom of the page.
     */
    if ($top >= $atTheBottom) {
        $socmedFAB.addClass('pulse');
        $socmedFAB.addClass('brown lighten-2');
        $socmedFAB.removeClass('blue-grey');
    }
    else {
        $socmedFAB.removeClass('pulse');
        $socmedFAB.removeClass('brown lighten-2');
        $socmedFAB.addClass('blue-grey');
    }

    /*
     I wanted to customize the hover behaviour, it's a great way to hide
     information and only display it when needed, perfect for the design.
     This .each() basically inflates each jsHoverTarget card to the size of
     its parent. Each jsHoverTarget is an absolutely positioned child card
     of a 'hoverable' card.
     */
    $('.jsHoverTarget').each(function () {
        var $this = $(this);
        var $card = $('.card'); //I think this is really dodgy. Luckily the first card returned is one of the ones we want
        var width = $card.outerWidth().toString();
        var height = $card.outerHeight().toString();

        $this.css('height', height);
        $this.css('width', width);

        $this.parent('.hoverable').hover(function () { //it might be time to get used to arrow notation
            $this.show();
        }, function () {
            $this.hide();
        });
    });
}
//End masterScrollFunction()

//'Scroll to top' click listener
$('.footer-icon').on('click', function(){
    window.scrollTo(0,0);
});

/*
 Quick and dirty way to position call to action text,
 around about 1/6 of the way in
 */
var $ctaTextDiv = $('.call-to-action-text');
var ctaTextLeft = $ctaTextDiv.siblings().outerWidth() / 6;
$ctaTextDiv.css('left', ctaTextLeft + 'px');

/*
 Initialize pushpins, '.each()' has been kept in case of future additions... the
 top parameter controls when pushpin nav is 'pinned', bottom when it is unpinned.
 We don't want it to unpin before the bottom of the page.
 */
$('.pushpin-nav').each(function () {
    var $this = $(this);
    var $target = $('#' + $(this).attr('data-target'));
    $this.pushpin({
        top: $target.offset().top - 64,
        bottom: 10000
    });
});

/*
 Create new Vivus object - handles SVG stroke animation for the sake of simplicity and modularity.
 $('.slide-logo').load('img/Bulgari_logo.svg'); unfortunately, dynamically loading SVGs into the
 HTML to clean things up doesn't work with vivus.
 */
var vivus = new Vivus('main-logo-outline-svg', {}, fadeInFill);
vivus.play(1);

/*
 Create callback function to be called once Vivus is finished animating:
 Apply fineInFill animation to increase fill opacity using CSS keyframes
 on a duplicate logo behind the Vivus logo - Vivus does not handle any
 paths with fills, so in order to have both we need to animate two logos.
 Thank the lord for function hoisting.
 */
function fadeInFill() {
    var $svg = $('#main-logo-fill-svg');
    $svg.css('fill', 'black');
    $svg.css('animation', 'fadeInFill 4s forwards'); //animation-fill-mode:forwards.
}

//----------- THE WASTELAND ---------------

/*
 These are some old snippets that I really enjoyed creating
 but either didn't work with the design of were just
 completely terrible. Mostly the latter.
 */


/*
 Shrink the hero div on page load.
 Way more obnoxious that it seems.
 */

/*
 var shrinkHero = setInterval(function () {
 if (height === 50){
 clearInterval(shrinkHero);
 }
 var $hero = $('.hero');
 $hero.css('height', height + 'vh');
 height--;
 }, 50);
 */

/*
 Create a kaleidoscopic effect in the navbar on scroll,
 using the top of the window and some mathery to change
 or randomize gradient RGB values. This was really
 fun to conceive and create. Shame it looks awful.
 All of it was tied up in the master scroll function.
 */


/*
 var slowIncrement = Math.floor(top / 8);
 var medIncrement = Math.floor(top / 6);
 var fastIncrement = Math.floor(top / 4);

 var l = [fastIncrement, medIncrement, slowIncrement];
 var r = function () {
 return Math.floor(Math.random() * 3)
 }
 */

/*
 $gradientDiv.css('background', 'linear-gradient(to right, rgb(' + med +
 ',' + slow +
 ',' + fast + ') 0%,rgb(' + fast +
 ',' + med +
 ',' + slow + ') 100%)');
 */

/*
 $gradientDiv.css('background', 'linear-gradient(to right, rgb(' + l[r] +
 ',' + l[r] +
 ',' + l[r] + ') 0%,rgb(' + l[r] +
 ',' + l[r] +
 ',' + l[r] + ') 100%)');
 */


